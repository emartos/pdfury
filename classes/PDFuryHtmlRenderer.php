<?php
namespace PDFury;

require_once(PDFURY_APPROOT . '/lib/twig/lib/Twig/Autoloader.php');

/**
 * PDFuryHtmlRenderer.php
 * Render content into HTML format
 * @package PDFury
 * @author Eduardo Martos
 */
class PDFuryHtmlRenderer {
    /**
     * @var string PDFury Template name
     */
    private $templateName;
    /**
     * @var mixed PDFury Template title
     */
    private $templateTitle;
    /**
     * @var array Content values
     */
    private $content;
    /**
     * @var string PDF language
     */
    private $lang;
    /**
     * @var string Output path
     */
    private $outputPath;
    /**
     * @var array Generated filenames
     */
    private $output;

    /**
     * Class constructor
     * @param $templateName
     * @param $templateTitle
     * @param $content
     * @param $lang
     */
    public function __construct($templateName, $templateTitle, $content, $lang) {
        $params = PDFuryParameters::getInstance();
        $tpl = $params->get('tpl');
        $this->outputPath = $params->get('output');
        $this->templateName = $templateName;
        $this->templateTitle =$templateTitle;
        $this->content = $content;
        $this->lang = $lang;
        $this->basePath = PDFURY_APPROOT . $tpl . $templateName . '/' . $lang . '/';
        $this->baseUrl = PDFURY_APPURL . $tpl . $templateName . '/' . $lang . '/';
    }

    /**
     * Generate the header/footer
     * @param $tplObj
     * @param $tpl
     * @param $suffix
     * @param $htmlHeader
     * @param $htmlFooter
     * @return bool|string
     */
    private function renderPart($tplObj, $tpl, $suffix, $htmlHeader, $htmlFooter) {
        $content = $htmlHeader . $tplObj->render($tpl, $this->content) . $htmlFooter;
        $outputFileName = $this->templateName . '-' . $suffix . '-' . date('d-m-Y_H-i') . '.html';
        $outputFile = PDFURY_APPROOT . $this->outputPath . $outputFileName;
        return PDFuryFile::write($outputFile, $content) ? $outputFileName : false;
    }

    /**
     * Insert the default header
     * @param $tplObj
     * @return mixed
     */
    private function renderHtmlHeader($tplObj) {
        $params = PDFuryParameters::getInstance();
        $cssPath = PDFURY_APPROOT . $params->get('tpl') . $this->templateName . '/' . $params->get('css');
        $cssUrl = PDFURY_APPURL . $params->get('tpl') . $this->templateName . '/' . $params->get('css');
        $content = array(
            'title' => $this->templateTitle,
            'css' => PDFuryFile::browseDirectory($cssPath, $cssUrl)
        );
        return $tplObj->render($params->get('html_header_tpl'), $content);
    }

    /**
     * Insert the default footer
     * @param $tplObj
     * @return mixed
     */
    private function renderHtmlFooter($tplObj) {
        $params = PDFuryParameters::getInstance();
        return $tplObj->render($params->get('html_footer_tpl'));
    }

    private function utf8Encode($string) {
        if (is_array($string)) {
            foreach ($string as $key => &$value) {
                $value = $this->utf8Encode($value);
            }
            $ret = $string;
        } else {
            $ret = utf8_encode($string);
        }
        return $ret;
    }

    /**
     * Render content into HTML format
     * @return bool|array
     */
    public function render() {
        $params = PDFuryParameters::getInstance();
        if (isset($this->content)) {
            \Twig_Autoloader::register();
            $loader = new \Twig_Loader_Filesystem($this->basePath);
            $tplObj = new \Twig_Environment($loader);
            $htmlHeader = $this->renderHtmlHeader($tplObj);
            $htmlFooter = $this->renderHtmlFooter($tplObj);
            $body = $htmlHeader . $tplObj->render($params->get('body_tpl'), $this->content) . $htmlFooter;
            $outputFileName = $this->templateName . '-' . date('d-m-Y_H-i') . '.html';
            $outputFile = PDFURY_APPROOT . $this->outputPath . $outputFileName;
            if (PDFuryFile::write($outputFile, $body)) {
                $this->output['headerFile'] = $this->renderPart($tplObj, $params->get('header_tpl'), 'header', $htmlHeader, $htmlFooter);
                $this->output['footerFile'] = $this->renderPart($tplObj, $params->get('footer_tpl'), 'footer', $htmlHeader, $htmlFooter);
                $this->output['bodyFile'] = $outputFileName;
                $ret = true;
            } else {
                $ret = false;
            }
        }
        return $ret;
    }

    /**
     * Retrieve the generated filenames
     * @return mixed
     */
    public function getOutput() {
        return ($this->output) ? $this->output : false;
    }
}