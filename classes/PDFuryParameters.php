<?php
namespace PDFury;

/**
 * PDFuryParameters.php
 * Parameters and configuration singleton
 * @package PDFury
 * @author Eduardo Martos
 */
final class PDFuryParameters {
    /**
     * Constant for attribute type text
     */
    const PDFURY_ATTR_TYPE_TEXT = 'PDFURY_ATTR_TYPE_TEXT';
    /**
     * Constant for attribute type date
     */
    const PDFURY_ATTR_TYPE_DATE = 'PDFURY_ATTR_TYPE_DATE';
    /**
     * Constant for attribute type currency
     */
    const PDFURY_ATTR_TYPE_CURRENCY = 'PDFURY_ATTR_TYPE_CURRENCY';
    /**
     * Constant for attribute type image
     */
    const PDFURY_ATTR_TYPE_IMAGE = 'PDFURY_ATTR_TYPE_IMAGE';
    /**
     * Constant for attribute type referenced field
     * If the field value is undefined, it takes the value of the referenced field
     */
    const PDFURY_ATTR_TYPE_REFERENCED_FIELD = 'PDFURY_ATTR_TYPE_REFERENCED_FIELD';
    /**
     * Constant for attribute type QR code
     * The attribute must be rendered to a QR code before sending to the renderer
     */
    const PDFURY_ATTR_TYPE_QR = 'PDFURY_ATTR_TYPE_QR';
    /**
     * Constant for attribute type codebase
     * The field value is translated using the Codebase functionality
     */
    const PDFURY_ATTR_TYPE_CODEBASE = 'PDFURY_ATTR_TYPE_CODEBASE';
    /**
     * Constant for attribute type unknown if undefined
     * If the field value is undefined, it takes the translation of 'undefined'
     */
    const PDFURY_ATTR_TYPE_UNKOWN_IF_UNDEFINED = 'PDFURY_ATTR_TYPE_UNKOWN_IF_UNDEFINED';
    /**
     * Constant for attribute type HTML
     * Transform HTML entities into readable characters
     */
    const PDFURY_ATTR_TYPE_HTML = 'PDFURY_ATTR_TYPE_HTML';
    /**
     * Constant for datasource type query
     */
    const PDFURY_DATASOURCE_QUERY = 'PDFURY_DATASOURCE_QUERY';
    /**
     * Constant for datasource type stored procedure
     */
    const PDFURY_DATASOURCE_PROCEDURE = 'PDFURY_DATASOURCE_PROCEDURE';

    /**
     * @var Array of parameters
     */
    private $params;

    /**
     * Private class constructor so nobody else can instance it
     */
    private function __construct() {}

    /**
     * Private class clone so nobody else can clone it
     */
    private function __clone() {}

    /**
     * Retrieve the persistent instance
     * @param string $configFile
     * @return \PDFuryIntegration
     */
    public static function getInstance($configFile = '') {
        static $inst = null;
        if ($inst === null) {
            $inst = new PDFuryParameters();
            // Load the configuration
            if ($config = file_get_contents($configFile)) {
                $inst->params = json_decode($config, true);
            }
        }
        return $inst;
    }

    /**
     * Retrieve the value of a parameter
     * @param $name
     * @return bool
     */
    public function get($name) {
        if (isset($this->params[$name])) {
            $ret = $this->params[$name];
        } else if (isset($_REQUEST[$name])) {
            $ret = $_REQUEST[$name];
        } else if (isset($_SESSION[$name])) {
            $ret = $_SESSION[$name];
        } else {
            $ret = false;
        }
        return $ret;
    }

    /**
     * Set the value of a parameter
     * @param $name
     * @param $value
     */
    public function set($name, $value) {
        $this->params[$name] = $value;
    }
}