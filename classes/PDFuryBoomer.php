<?php
namespace PDFury;

/**
 * PDFuryBoomer.php
 * Class to retrieve the data for the PDF
 * @package PDFury
 * @author Eduardo Martos
 */
class PDFuryBoomer {
    /**
     * @var int PDFury Template ID
     */
    private $pdfuryTemplateID;
    /**
     * @var array Query filters
     */
    private $filters;
    /**
     * @var string PDF language
     */
    private $lang;
    /**
     * @var array Conditional sections
     */
    private $conditionalSections;

    /**
     * Class constructor
     * @param int $pdfuryTemplateID
     * @param array $filters
     * @param string $lang
     * @param array $conditionalSections
     */
    public function __construct($pdfuryTemplateID, $filters, $lang, $conditionalSections = array()) {
        $this->pdfuryTemplateID = $pdfuryTemplateID;
        $this->filters = $filters;
        $this->lang = $lang;
        $this->conditionalSections = $conditionalSections;
    }

    /**
     * Retrieve the data
     * @return mixed
     */
    public function retrieve() {
        $integration = PDFuryIntegration::getInstance();
        $attributes = $integration->getAttributes($this->pdfuryTemplateID);
        $datasources = $integration->getDatasources($this->pdfuryTemplateID);
        $data = $this->getAttributesValues($datasources, $this->filters);
        $resArray = array();
        if (is_array($data) && count($data) &&
            is_array($attributes) && count($attributes)) {
            foreach ($data as $key => $valuesArray) {
                foreach ($valuesArray as $attributeName => $value) {
                    // As we can have multiple datasources in the same array, we have to check if the attribute does exist
                    if (isset($attributes[$attributeName]) && $attributes[$attributeName]['Attribute_Name']) {
                        $attribute = $attributes[$attributeName];
                        // Referenced attributes are self-reflexive
                        if ($attribute['Attribute_Type'] == PDFuryParameters::PDFURY_ATTR_TYPE_REFERENCED_FIELD) {
                            $referencedAttribute = $integration->getAttributes($this->pdfuryTemplateID, $attribute['Referenced_Attribute_ID']);
                            if ($referencedAttribute[0]) {
                                $attribute = $referencedAttribute[0];
                            }
                        }
                        // Check the attribute type
                        $value = $this->transformAttribute($attribute['Attribute_Type'], $value);
                        // Fill the resultant array
                        $this->fillResultantArray($attribute, $resArray, $value);
                    }
                }
            }
            $resArray = $this->clearResultantArray($resArray);
        }
        return $resArray;
    }

    /**
     * Execute the datasources and retrieve all the data
     * @param array $datasources
     * @param array $filters
     * @return array
     */
    private function getAttributesValues($datasources, $filters) {
        $data = array();
        if (is_array($datasources)) {
            foreach ($datasources as $singleDatasource) {
                $datasource = $singleDatasource['Datasource'];
                $datasourceType = $singleDatasource['Datasource_Type'];
                if ($datasourceType == PDFuryParameters::PDFURY_DATASOURCE_QUERY) {
                    $newData = $this->executeQuery($datasource, $filters);
                } else if ($datasourceType == PDFuryParameters::PDFURY_DATASOURCE_QUERY) {
                    $newData = $this->executeProcedure($datasource, $filters);
                }
                if (is_array($newData)) {
                    $data = array_merge($data, $newData);
                }
            }
        }
        return $data;
    }

    /**
     * Execute a SQL query
     * @param string $queryString
     * @param array $filters
     * @return array|bool
     */
    private function executeQuery($queryString, $filters) {
        $integration = PDFuryIntegration::getInstance();
        $data = $integration->query($this->prepareQuery($queryString, $filters));
        return $data;
    }

    /**
     * Execute a stored procedure
     * @param string $procedureName
     * @param array $filters
     * @return Array
     */
    private function executeProcedure($procedureName, $filters) {
        $integration = PDFuryIntegration::getInstance();
        $data = $integration->callProcedure($procedureName, $filters);
        return $data;
    }

    /**
     * Clean the query and set the filters
     * @param string $datasource
     * @param array $filters
     * @return mixed
     */
    private function prepareQuery($datasource, $filters) {
        $token = ':';
        // Remove the SELECT if defined
        if (strpos($datasource, 'SELECT ') == 0) {
            $datasource = substr($datasource, 6);
        }
        // Set the filters if defined
        if (is_array($filters) && count($filters)) {
            foreach ($filters as $key => $value) {
                $datasource = str_replace($token . $key . $token, $value, $datasource);
            }
        }
        return $datasource;
    }

    /**
     * Transform an attribute based on its type
     * @param string $attributeType
     * @param string $value
     * @return String
     */
    private function transformAttribute($attributeType, $value) {
        switch ($attributeType) {
            // Translate the value from codebase
            case PDFuryParameters::PDFURY_ATTR_TYPE_CODEBASE:
                $integration = PDFuryIntegration::getInstance();
                $value = $integration->getCodebase($value, $this->lang);
                break;
            // Call the QR builder method
            case PDFuryParameters::PDFURY_ATTR_TYPE_QR:
                $value = 'https://chart.googleapis.com/chart?cht=qr&chl=' . urlencode($value) . '&choe=UTF-8&chs=300x300';
                break;
            // Change the values of attribute from the another reference (via Query object)
            case PDFuryParameters::PDFURY_ATTR_TYPE_REFERENCED_FIELD:
                break;
            // Check if undefined and retrieve the translation for 'unknown' from codebase in that case
            case PDFuryParameters::PDFURY_ATTR_TYPE_UNKOWN_IF_UNDEFINED:
                if (empty($value)) {
                    $integration = PDFuryIntegration::getInstance();
                    $value = $integration->getCodebase('unknown', $this->lang);
                }
                break;
            // Add relative URL to images
            case PDFuryParameters::PDFURY_ATTR_TYPE_IMAGE:
                // Check if URL is valid
                $value = PDFuryFile::checkUrl($value);
                // Check if URL is image
                if ($value && !getimagesize($value)) {
                    $value = '';
                }
                break;
            case PDFuryParameters::PDFURY_ATTR_TYPE_CURRENCY:
                $integration = PDFuryIntegration::getInstance();
                $value = $integration->getFormattedCurrency($value);
                break;
            case PDFuryParameters::PDFURY_ATTR_TYPE_DATE:
                $integration = PDFuryIntegration::getInstance();
                $value = $integration->getFormattedDate($value);
                break;
            case PDFuryParameters::PDFURY_ATTR_TYPE_HTML:
                $value = strip_tags($value);
                $value = html_entity_decode($value);
                break;
        }
        return $value;
    }

    /**
     * Fill the resultant array
     * @param array $attribute
     * @param array $resArray
     * @param string $value
     */
    private function fillResultantArray($attribute, &$resArray, $value) {
        $section = isset($attribute['Section_Name']) ? $attribute['Section_Name'] : '';
        $container = isset($attribute['Container_Name']) ? $attribute['Container_Name'] : '';
        $attributeName = $attribute['Attribute_Name'];
        // Template name
        $resArray['template_name'] = isset($attribute['Template_Name']) ? $attribute['Template_Name'] : '';
        $resArray['template_title'] = isset($attribute['Template_Title']) ? $attribute['Template_Title'] : '';
        $resArray['pdf_command_params'] = isset($attribute['PDF_Command_Parameters']) ? $attribute['PDF_Command_Parameters'] : '';
        // It's inside a section and a container
        if (!empty($section) && !empty($container)) {
            $show = true;
            if (isset($this->conditionalSections[$section]) || isset($this->conditionalSections[$container])) {
                $show = $this->conditionalSections[$section] || $this->conditionalSections[$container];
            }
            if ($show) {
                $resArray['content'][$section][$container][$attributeName][] = $value;
            }
            // It's inside a section and outside any container
        } else if (!empty($section) && empty($container)) {
            $show = true;
            if (isset($this->conditionalSections[$section])) {
                $show = $this->conditionalSections[$section];
            }
            if ($show) {
                $resArray['content'][$section][$attributeName][] = $value;
            }
            // It's outside any section and inside a container
        } else if (empty($section) && !empty($container)) {
            $show = true;
            if (isset($this->conditionalSections[$container])) {
                $show = $this->conditionalSections[$container];
            }
            if ($show) {
                $resArray['content'][$container][$attributeName][] = $value;
            }
            // It's outside any section and container
        } else {
            if (!empty($attributeName)) {
                $resArray['content'][$attributeName][] = $value;
            }
        }
    }

    /**
     * Clear the final array
     * @param $resArray
     * @return mixed
     */
    private function clearResultantArray($resArray) {
        foreach ($resArray['content'] as $key => &$item) {
            if (count($item) == 1 && !is_array(array_values($item)[0])) {
                $value = array_values($item);
                $item = array_shift($value);
            }
        }
        return $resArray;
    }
}