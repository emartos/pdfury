<?php
namespace PDFury;

/**
 * PDFuryFile.php
 * File helper
 * @package PDFury
 * @author Eduardo Martos
 */
class PDFuryFile {
    /**
     * Check if a file exists
     * @param $filename
     * @return bool
     */
    public static function exists($filename) {
        return file_exists($filename);
    }

    /**
     * Read a file
     * @param $filename
     * @return bool|string
     */
    public static function read($filename) {
        $ret = false;
        if (file_exists($filename)) {
            if (($fp = fopen($filename, 'r')) !== false) {
                $ret = fread($fp, filesize($filename));
            }
        }
        return $ret;
    }

    /**
     * Write a file
     * @param $filename
     * @param $content
     * @return bool|int
     */
    public static function write($filename, $content) {
        $ret = false;
        if (!file_exists($filename)) {
            touch($filename);
        }
        if (chmod($filename, 0760)) {
            if (($fp = fopen($filename, 'w')) !== false) {
                $ret = fwrite($fp, $content);
            }
        }
        return $ret;
    }

    /**
     * Append content to a file
     * @param $filename
     * @param $content
     * @return bool|int
     */
    public static function append($filename, $content) {
        $ret = false;
        if (!file_exists($filename)) {
            touch($filename);
        }
        if (chmod($filename, 0770)) {
            if (($fp = fopen($filename, 'a')) !== false) {
                $ret = fwrite($fp, $content);
            }
        }
        return $ret;
    }

    /**
     * Delete a file
     * @param $filename
     * @return bool
     */
    public static function delete($filename) {
        return unlink($filename);
    }

    /**
     * Clear a folder
     * @param $path
     */
    public static function clearFolder($path) {
        array_map('unlink', glob($path));
    }

    /**
     * Browse a directory and return the content
     * @param $dir
     * @param string $prefix
     * @return array
     */
    public static function browseDirectory($dir, $prefix = '') {
        $ret = '';
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file != '.' && $file != '..') {
                        $ret[] = $prefix . $file;
                    }
                }
                closedir($dh);
            }
        }
        return $ret;
    }

    /**
     * Retrieve the absolute URL
     * @return string
     */
    public static function getUrl() {
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, strpos( $_SERVER["SERVER_PROTOCOL"], '/')));
        $serverName = $_SERVER['SERVER_NAME'] . ($_SERVER['SERVER_PORT'] != '80' ? ':' . $_SERVER['SERVER_PORT'] : '');
        $basepath = dirname($_SERVER['PHP_SELF']);
        $url = $protocol . '://' . $serverName . $basepath . '/';
        return $url;
    }

    /**
     * Check if a URL is valid and retrieve it with the absolute part
     * @param $url
     * @return string
     */
    public static function checkUrl($url) {
        list($status) = get_headers(self::getUrl() . $url);
        $url = (strpos($status, '404') === false) ? self::getUrl() . $url : false;
        return $url;
    }

    /**
     * Download a file
     *
     * @param $file
     */
    public static function download($file) {
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: Binary');
        header('Content-disposition: attachment; filename="' . basename($file) . '"');
        readfile($file);
    }
}

