<?php
namespace PDFury;

/**
 * PDFuryIntegration.php
 * Integration fa�ade
 * @package PDFury
 * @author Eduardo Martos
 */
final class PDFuryIntegration {
    /**
     * Private class constructor so nobody else can instance it
     */
    private function __construct() {}

    /**
     * Private class clone so nobody else can clone it
     */
    private function __clone() {}

    /**
     * Retrieve the persistent instance
     * @return \PDFuryIntegration
     * @throws \Exception
     */
    public static function getInstance() {
        static $inst = null;
        if ($inst === null) {
            $params = PDFuryParameters::getInstance();
            $integrationPoint = '\\PDFury\\' . $params->get('integration_point');
            if (class_exists($integrationPoint)) {
                $inst = new $integrationPoint();
            } else {
                throw new \Exception('[PDFury] Integration point not found');
            }
        }
        return $inst;
    }
}