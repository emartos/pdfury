<?php
namespace PDFury;

/**
 * IPDFuryIntegration.php
 * Interface IPDFuryIntegration
 * @package PDFury
 * @author Eduardo Martos
 */
interface IPDFuryIntegration {
    /**
     * Execute a query and retrieve the data
     * @param $queryString
     * @return mixed
     */
    public function query($queryString);

    /**
     * Execute a stored procedure and retrieve the data
     * @param $procedureName
     * @param array $filters
     * @return mixed
     */
    public function callProcedure($procedureName, $filters = array());

    /**
     * Retrieve a translation for a term
     * @param $value
     * @param $lang
     * @return mixed
     */
    public function getTranslation($value, $lang);

    /**
     * Retrieve a core translation for a term
     * @param $value
     * @param $lang
     * @return mixed
     */
    public function getCodebase($value, $lang);

    /**
     * Retrieve a formatted currency
     * @param $value
     * @return mixed
     */
    public function getFormattedCurrency($value);

    /**
     * Retrieve a formatted date
     * @param $value
     * @return mixed
     */
    public function getFormattedDate($value);

    /**
     * Retrieve the template ID from the template name
     * @param $templateName
     * @return mixed
     */
    public function getTemplateID($templateName);

    /**
     * Retrieve the attributes associated to a template
     * @param $pdfuryTemplateID
     * @return mixed
     */
    public function getAttributes($pdfuryTemplateID);

    /**
     * Retrieve the datasources associated to a template
     * @param $pdfuryTemplateID
     * @return mixed
     */
    public function getDatasources($pdfuryTemplateID);
}