<?php
namespace PDFury;

/**
 * PDFuryFCM.php
 * Integration fa�ade
 * @package PDFury
 * @author Eduardo Martos
 */
class PDFuryFCM implements IPDFuryIntegration {
    /**
     * Execute a query and retrieve the data
     * @param $queryString
     * @return mixed
     */
    public function query($queryString) {
        global $mysql, $trans;
        $query = new \query($mysql, $trans, \query::$SELECT);
        $query->addValue($queryString);
        $data = $query->queryFetchAssoc();
        return $data;
    }

    /**
     * Execute a stored procedure and retrieve the data
     * @param $procedureName
     * @param array $filters
     * @return mixed
     */
    public function callProcedure($procedureName, $filters = array()) {
        $procedure = new \Call_Procedure_Query();
        $procedure->set_Mainlist($procedureName);
        $procedure->set_Call_Filter($filters);
        return $procedure->execute_Procedure();
    }

    /**
     * Retrieve a translation for a term
     * @param $value
     * @param $lang
     * @return mixed
     */
    public function getTranslation($value, $lang) {
        $ret = false;
        if ($value && $lang) {
            $queryString = $lang . " FROM language WHERE Name='" . $value . "'";
            $data = $this->query($queryString);
            if ($data) {
                $data = $data[0];
                $ret = $data[$lang];
            }
        }
        return $ret;
    }

    /**
     * Retrieve a core translation for a term
     * @param $value
     * @param $lang
     * @return mixed
     */
    public function getCodebase($value, $lang) {
        $ret = false;
        if ($value && $lang) {
            $queryString = $lang . " FROM codebase WHERE Translation_Code='" . $value . "'";
            $data = $this->query($queryString);
            if ($data) {
                $data = $data[0];
                $ret = $data[$lang];
            }
        }
        return $ret;
    }

    /**
     * Retrieve a formatted currency
     * @param $value
     * @return mixed
     */
    public function getFormattedCurrency($value) {
        $currency = new \Currency_Query();
        $currency->load(\Functions::get_User_Currency_ID());
        $value = \Functions::get_Language_Formatted_Currency($value, $currency->get_Symbol(), $currency->get_Possition());
        $value = str_replace('&nbsp;', ' ', $value);
        return $value;
    }

    /**
     * Retrieve a formatted date
     * @param $value
     * @return mixed
     */
    public function getFormattedDate($value) {
        return \Functions::get_UserFormated_DateTime($value);
    }

    /**
     * Retrieve the template ID from the template name
     * @param $templateName
     * @return mixed
     */
    public function getTemplateID($templateName) {
        $pdfuryLogic = new PDFuryLogic();
        return $pdfuryLogic->getTemplateID($templateName);
    }

    /**
     * Retrieve the attributes associated to a template
     * @param $pdfuryTemplateID
     * @return mixed
     */
    public function getAttributes($pdfuryTemplateID) {
        $pdfuryLogic = new PDFuryLogic();
        return $pdfuryLogic->getAttributes($pdfuryTemplateID);
    }

    /**
     * Retrieve the datasources associated to a template
     * @param $pdfuryTemplateID
     * @return mixed
     */
    public function getDatasources($pdfuryTemplateID) {
        $pdfuryLogic = new PDFuryLogic();
        return $pdfuryLogic->getDatasources($pdfuryTemplateID);
    }
}