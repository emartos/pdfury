<?php
namespace PDFury;

/**
 * PDFuryPdfRenderer.class.php
 * Convert HTML content into PDF
 * @package PDFury
 * @author Eduardo Martos
 */
class PDFuryPdfRenderer {
    /**
     * @var string PDFury Template name
     */
    private $templateName;
    /**
     * @var string Body template filename
     */
    private $bodyFile;
    /**
     * @var string Header template filename
     */
    private $headerFile;
    /**
     * @var string Footer template filename
     */
    private $footerFile;

    /**
     * Class constructor
     * @param $templateName
     * @param $bodyFile
     * @param $headerFile
     * @param $footerFile
     */
    public function __construct($templateName, $bodyFile, $headerFile, $footerFile) {
        $this->templateName = $templateName;
        $this->bodyFile = $bodyFile;
        $this->headerFile = $headerFile;
        $this->footerFile = $footerFile;
    }

    /**
     * Save the content into PDF
     * @return bool
     */
    public function render() {
        $ret = false;
        $params = PDFuryParameters::getInstance();
        $outputPath = PDFURY_APPROOT . $params->get('output');
        $outputUrl = PDFURY_APPURL . $params->get('output');
        if (PDFuryFile::exists($outputPath . $this->bodyFile)) {
            $outputFileName = $this->templateName . '-' . date('d-m-Y_H-i') . '.pdf';
            $outputFile = $outputPath . $outputFileName;
            $headerParam = !empty($this->headerFile) ?
                $headerParam = ' --header-html "' . $outputUrl . $this->headerFile . '" ' : '';
            $footerParam = !empty($this->footerFile) ?
                $footerParam = ' --footer-html "' . $outputUrl . $this->footerFile . '" ' : '';
            $exec = $params->get('pdf_command') . ' ' . $params->get('pdf_command_params') . ' ' . $headerParam . $footerParam . '"' . $outputUrl .
                    $this->bodyFile . '" "' . $outputFile . '"';
            exec($exec, $output, $ret);
            if (PDFuryFile::exists($outputFile)) {
                $ret = $outputFileName;
            }
        }
        return $ret;
    }
}