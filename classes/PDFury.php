<?php
namespace PDFury;

/**
 * PDFury.php
 * Class to render PDFs
 * @package PDFury
 * @author Eduardo Martos
 */
class PDFury {
    /**
     * @var string PDFury Template name
     */
    private $templateName;
    /**
     * @var int PDFury Template ID
     */
    private $templateId;
    /**
     * @var string PDF language
     */
    private $lang;
    /**
     * @var array Query filters
     */
    private $filters;
    /**
     * @var array Conditional sections
     */
    private $conditionalSections;
    /**
     * @var Generated PDF filename
     */
    private $generatedPdf;

    /**
     * Class constructor
     * @param $templateName
     * @param $lang
     * @param $filters
     * @param $conditionalSections
     */
    public function __construct($templateName, $lang, $filters = array(), $conditionalSections = array()) {
        // Define constants
        if (!defined('PDFURY_APPROOT')) define('PDFURY_APPROOT', dirname($_SERVER['SCRIPT_FILENAME']) . '/Tools/PDFury/');
        if (!defined('PDFURY_APPURL')) define('PDFURY_APPURL', PDFuryFile::getUrl() . 'Tools/PDFury/');
        // Load configuration values
        PDFuryParameters::getInstance(PDFURY_APPROOT . 'config.json');
        // Load class attributes
        $integration = PDFuryIntegration::getInstance();
        $this->templateName = $templateName;
        $this->templateId = $integration->getTemplateID($templateName);
        $this->lang = strtolower($lang);
        $this->filters = $filters;
        $this->conditionalSections = $conditionalSections;
    }

    /**
     * Render the content
     * @return bool
     */
    public function render() {
        $ret = false;
        try {
            $pdfuryBoomer = new PDFuryBoomer($this->templateId, $this->filters, $this->lang, $this->conditionalSections);
            if ($parameters = $pdfuryBoomer->retrieve()) {
               // Render the content into HTML
                if (!$this->checkTplFolder($this->templateName)) {
                    $integration = PDFuryIntegration::getInstance();
                    print $integration->getTranslation('file_not_found', $this->lang);
                    return false;
                } else {
                    $parameters['content']['appurl'] = PDFuryFile::getUrl();
                    $parameters['content']['title'] = $parameters['template_title'];
                    // Override the default parameters for the PDF command
                    if (isset($parameters['pdf_command_params']) && !empty($parameters['pdf_command_params'])) {
                        $params = PDFuryParameters::getInstance();
                        $params->set('pdf_command_params', $parameters['pdf_command_params']);
                    }
                    // Clear the output folder
                    $params = PDFuryParameters::getInstance();
                    $outputPath = PDFURY_APPROOT . $params->get('output');
                    PDFuryFile::clearFolder($outputPath . '*.pdf');
                    PDFuryFile::clearFolder($outputPath . '*.html');
                    // Render the document
                    $htmlRenderer = new PDFuryHtmlRenderer($this->templateName, $parameters['template_title'], $parameters['content'], $this->lang);
                    if ($htmlRenderer->render() !== false) {
                        $htmlOutput = $htmlRenderer->getOutput();
                        // Render the content into PDF
                        $pdfRenderer = new PDFuryPdfRenderer($this->templateName, $htmlOutput['bodyFile'],
                                                             $htmlOutput['headerFile'], $htmlOutput['footerFile']);
                        if ($ret = $pdfRenderer->render()) {
                            $this->generatedPdf = $ret;
                            $ret = true;
                        }
                        PDFuryFile::delete($outputPath . $htmlOutput['bodyFile']);
                        PDFuryFile::delete($outputPath . $htmlOutput['headerFile']);
                        PDFuryFile::delete($outputPath . $htmlOutput['footerFile']);
                    }
                }
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
        return $ret;
    }

    /**
     * Download the PDF document
     */
    public function download() {
        if ($this->render()) {
            PDFuryFile::download($this->getGeneratedPdfPath());
        }
    }

    /**
     * Check the templates folder and fix the lang
     * @param $templateName
     * @return bool|string
     */
    private function checkTplFolder($templateName) {
        $params = PDFuryParameters::getInstance();
        $basepath = PDFURY_APPROOT . $params->get('tpl') . $templateName . '/';
        if (is_dir($basepath . strtolower($this->lang))) {
            $this->lang = strtolower($this->lang);
            $ret = true;
        } else if (is_dir($basepath . strtoupper($this->lang))) {
            $this->lang = strtoupper($this->lang);
            $ret = true;
        } else {
            $ret = false;
        }
        return $ret;
    }

    /**
     * Retrieve the path of the generated PDF
     * @return mixed
     */
    public function getGeneratedPdfPath() {
        $params = PDFuryParameters::getInstance();
        $outputPath = PDFURY_APPROOT . $params->get('output');
        return $outputPath . $this->generatedPdf;
    }

    /**
     * Retrieve the URL of the generated PDF
     * @return mixed
     */
    public function getGeneratedPdfUrl() {
        $params = PDFuryParameters::getInstance();
        $outputUrl = PDFURY_APPURL . $params->get('output');
        return $outputUrl . $this->generatedPdf;
    }

    /**
     * Retrieve the defined languages inside the template
     * @param $templateName
     * @return array
     */
    public function getDefinedLanguages($templateName) {
        $params = PDFuryParameters::getInstance();
        $basepath = PDFURY_APPROOT . $params->get('tpl') . $templateName . '/';
        $langs = PDFuryFile::browseDirectory($basepath);
        if(($key = array_search('css', $langs)) !== false) {
            unset($langs[$key]);
        }
        foreach($langs as &$lang) {
            $lang = strtolower($lang);
        }
        return $langs;
    }
}