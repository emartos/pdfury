<?php
namespace PDFury;

/**
 * Class PdfuryTemplateToDatasourceQuery
 * @package PDFury
 * @author Eduardo Martos
 */
class PdfuryTemplateToDatasourceQuery extends FCM_Query {
    /**
     * @var int $pdfuryTemplateID Database type: int; Database column: PDFury_Template_ID
     */
    protected $pdfuryTemplateID;
    /**
     * @var int $pdfuryDatasourceID Database type: int; Database column: PDFury_Datasource_ID
     */
    protected $pdfuryDatasourceID;

    /**
     * Constructor for class PdfuryTemplateToDatasourceQuery
     * @param bool|int $primaryValue Primary key value
     */
    public function __construct($primaryValue = false) {
        parent::__construct($primaryValue);

        // Table name
        $this->table = 'pdfury_template_to_datasource';

        // Primary key
        $this->primary_key = 'PDFury_Template_To_Datasource_ID';

        // Columns
        $this->pdfuryTemplateID = 'PDFury_Template_ID';
        $this->pdfuryDatasourceID = 'PDFury_Datasource_ID';

        // Columns
        $this->fieldlist[$this->pdfuryTemplateID] = '';
        $this->fieldlist[$this->pdfuryDatasourceID] = '';

        // Null fields

    }

    /**
     * Set the pdfuryTemplateID in $this->fieldlist
     * @param int $value Database type: int; Database column: PDFury_Template_ID
     */
    public function setPDFuryTemplateID($value) {
        $this->fieldlist[$this->pdfuryTemplateID] = $value;
    }

    /**
     * Return the pdfuryTemplateID from $this->fieldlist
     * @return int Database type: int; Database column: PDFury_Template_ID
     */
    public function getPDFuryTemplateID() {
        return $this->fieldlist[$this->pdfuryTemplateID];
    }

    /**
     * Set the pdfuryDatasourceID in $this->fieldlist
     * @param int $value Database type: int; Database column: PDFury_Datasource_ID
     */
    public function setPDFuryDatasourceID($value) {
        $this->fieldlist[$this->pdfuryDatasourceID] = $value;
    }

    /**
     * Return the pdfuryDatasourceID from $this->fieldlist
     * @return int Database type: int; Database column: PDFury_Datasource_ID
     */
    public function getPDFuryDatasourceID() {
        return $this->fieldlist[$this->pdfuryDatasourceID];
    }
}