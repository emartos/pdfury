<?php
namespace PDFury;

/**
 * Class PdfuryDatasourceQuery
 * @package PDFury
 * @author Eduardo Martos
 */
class PdfuryDatasourceQuery extends FCM_Query {
    /**
     * @var String $attributeType Database type: text; Database column: Datasource
     */
    protected $datasource;
    /**
     * @var String $datasourceType Database type: varchar; Database column: Datasource_Type
     */
    protected $datasourceType;

    /**
     * Constructor for class PdfuryDatasourceQuery
     * @param bool|int $primaryValue Primary key value
     */
    public function __construct($primaryValue = false) {
        parent::__construct($primaryValue);

        // Table name
        $this->table = 'pdfury_datasource';

        // Primary key
        $this->primary_key = 'PDFury_Datasource_ID';

        // Columns
        $this->datasource = 'Datasource';
        $this->datasourceType = 'Datasource_Type';

        // Columns
        $this->fieldlist[$this->attributeType] = '';

        // Null fields

    }

    /**
     * Set the datasource in $this->fieldlist
     * @param String $value Database type: text; Database column: Datasource
     */
    public function setDatasource($value) {
        $this->fieldlist[$this->datasource] = $value;
    }

    /**
     * Return the datasource from $this->fieldlist
     * @return String Database type: text; Database column: Datasource
     */
    public function getDatasource() {
        return $this->fieldlist[$this->datasource];
    }

    /**
     * Set the datasourceType in $this->fieldlist
     * @param String $value Database type: varchar; Database column: Datasource_Type
     */
    public function setDatasourceType($value) {
        $this->fieldlist[$this->datasourceType] = $value;
    }

    /**
     * Return the datasourceType from $this->fieldlist
     * @return String Database type: varchar; Database column: Datasource_Type
     */
    public function getDatasourceType() {
        return $this->fieldlist[$this->datasourceType];
    }
}