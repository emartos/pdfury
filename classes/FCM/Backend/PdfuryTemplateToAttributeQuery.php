<?php
namespace PDFury;

/**
 * Class PdfuryTemplateToAttributeQuery
 * @package PDFury
 * @author Eduardo Martos
 */
class PdfuryTemplateToAttributeQuery extends FCM_Query {
    /**
     * @var int $pdfuryTemplateID Database type: int; Database column: PDFury_Template_ID
     */
    protected $pdfuryTemplateID;
    /**
     * @var int $pdfuryAttributeID Database type: int; Database column: PDFury_Attribute_ID
     */
    protected $pdfuryAttributeID;
    /**
     * @var int $pdfurySectionID Database type: int; Database column: PDFury_Section_ID
     */
    protected $pdfurySectionID;
    /**
     * @var String $pdfuryContainerID Database type: int; Database column: PDFury_Container_ID
     */
    protected $pdfuryContainerID;

    /**
     * Constructor for class PdfuryTemplateToAttribute
     * @param bool|int $primaryValue Primary key value
     */
    public function __construct($primaryValue = false) {
        parent::__construct($primaryValue);

        // Table name
        $this->table = 'pdfury_template_to_attribute';

        // Primary key
        $this->primary_key = 'PDFury_Template_To_Attribute_ID';

        // Columns
        $this->pdfuryTemplateID = 'PDFury_Template_ID';
        $this->pdfuryAttributeID = 'PDFury_Attribute_ID';
        $this->pdfuryContainerID = 'PDFury_Section_ID';
        $this->pdfuryContainerID = 'PDFury_Container_ID';

        // Columns
        $this->fieldlist[$this->pdfuryTemplateID] = '';
        $this->fieldlist[$this->pdfuryAttributeID] = '';

        // Null fields

    }

    /**
     * Set the pdfuryTemplateID in $this->fieldlist
     * @param int $value Database type: int; Database column: PDFury_Template_ID
     */
    public function setPDFuryTemplateID($value) {
        $this->fieldlist[$this->pdfuryTemplateID] = $value;
    }

    /**
     * Return the pdfuryTemplateID from $this->fieldlist
     * @return int Database type: int; Database column: PDFury_Template_ID
     */
    public function getPDFuryTemplateID() {
        return $this->fieldlist[$this->pdfuryTemplateID];
    }

    /**
     * Set the pdfuryAttributeID in $this->fieldlist
     * @param int $value Database type: int; Database column: PDFury_Attribute_ID
     */
    public function setPDFuryAttributeID($value) {
        $this->fieldlist[$this->pdfuryAttributeID] = $value;
    }

    /**
     * Return the pdfuryAttributeID from $this->fieldlist
     * @return int Database type: int; Database column: PDFury_Attribute_ID
     */
    public function getPDFuryAttributeID() {
        return $this->fieldlist[$this->pdfuryAttributeID];
    }

    /**
     * Set the pdfurySectionID in $this->fieldlist
     * @param int $value Database type: int; Database column: PDFury_Section_ID
     */
    public function setPDFurySectionID($value) {
        $this->fieldlist[$this->pdfurySectionID] = $value;
    }

    /**
     * Return the pdfurySectionID from $this->fieldlist
     * @return int Database type: int; Database column: PDFury_Section_ID
     */
    public function getPDFurySectionID() {
        return $this->fieldlist[$this->pdfurySectionID];
    }

    /**
     * Set the pdfuryContainerID in $this->fieldlist
     * @param int $value Database type: int; Database column: PDFury_Container_ID
     */
    public function setPDFuryContainerID($value) {
        $this->fieldlist[$this->pdfuryContainerID] = $value;
    }

    /**
     * Return the pdfuryContainerID from $this->fieldlist
     * @return int Database type: int; Database column: PDFury_Container_ID
     */
    public function getPDFuryContainerID() {
        return $this->fieldlist[$this->pdfuryContainerID];
    }
}