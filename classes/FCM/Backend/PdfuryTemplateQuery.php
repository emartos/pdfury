<?php
namespace PDFury;

/**
 * Class PdfuryTemplateQuery
 * @package PDFury
 * @author Eduardo Martos
 */
class PdfuryTemplateQuery extends FCM_Query {
    /**
     * @var String $templateName Database type: varchar; Database column: Template_Name
     */
    protected $templateName;
    /**
     * @var String $templateTitle Database type: varchar; Database column: Template_Title
     */
    protected $templateTitle;

    /**
     * Constructor for class PdfuryTemplate
     * @param bool|int $primaryValue Primary key value
     */
    public function __construct($primaryValue = false) {
        parent::__construct($primaryValue);

        // Table name
        $this->table = 'pdfury_template';

        // Primary key
        $this->primary_key = 'PDFury_Template_ID';

        // Columns
        $this->templateName = 'Template_Name';

        // Columns
        $this->fieldlist[$this->templateName] = '';

        // Null fields

    }

    /**
     * Set the templateName in $this->fieldlist
     * @param String $value Database type: varchar; Database column: Template_Name
     */
    public function setTemplateName($value) {
        $this->fieldlist[$this->templateName] = $value;
    }

    /**
     * Return the templateName from $this->fieldlist
     * @return String Database type: varchar; Database column: Template_Name
     */
    public function getTemplateName() {
        return $this->fieldlist[$this->templateName];
    }

    /**
     * Set the templateTitle in $this->fieldlist
     * @param String $value Database type: varchar; Database column: Template_Title
     */
    public function setTemplateTitle($value) {
        $this->templateTitle = $value;
    }

    /**
     * Return the templateTitle from $this->fieldlist
     * @return String Database type: varchar; Database column: Template_Title
     */
    public function getTemplateTitle() {
        return $this->templateTitle;
    }
}