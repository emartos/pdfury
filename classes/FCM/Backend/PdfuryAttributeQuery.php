<?php
namespace PDFury;

/**
 * Class PdfuryAttributeQuery
 * @package PDFury
 * @author Eduardo Martos
 */
class PdfuryAttributeQuery extends FCM_Query {
    /**
     * @var int $pdfuryAttributeTypeID Database type: int; Database column: PDFury_Attribute_Type_ID
     */
    protected $pdfuryAttributeTypeID;
    /**
     * @var String $attributeName Database type: varchar; Database column: Attribute_Name
     */
    protected $attributeName;
    /**
     * @var int $referencedAttributeID Database type: int; Database column: Referenced_Attribute_ID
     */
    protected $referencedAttributeID;

    /**
     * Constructor for class PdfuryAttribute
     * @param bool|int $primaryValue Primary key value
     */
    public function __construct($primaryValue = false) {
        parent::__construct($primaryValue);

        // Table name
        $this->table = 'pdfury_attribute';

        // Primary key
        $this->primary_key = 'PDFury_Attribute_ID';

        // Columns
        $this->pdfuryAttributeTypeID = 'PDFury_Attribute_Type_ID';
        $this->attributeName = 'Attribute_Name';
        $this->referencedAttributeID = 'Referenced_Attribute_ID';

        // Columns
        $this->fieldlist[$this->pdfuryAttributeTypeID] = '';
        $this->fieldlist[$this->attributeName] = '';
        $this->fieldlist[$this->referencedAttributeID] = '';

        // Null fields

    }

    /**
     * Set the pdfuryAttributeTypeID in $this->fieldlist
     * @param int $value Database type: int; Database column: PDFury_Attribute_Type_ID
     */
    public function setPDFuryAttributeTypeID($value) {
        $this->fieldlist[$this->pdfuryAttributeTypeID] = $value;
    }

    /**
     * Return the pdfuryAttributeTypeID from $this->fieldlist
     * @return int Database type: int; Database column: PDFury_Attribute_Type_ID
     */
    public function getPDFuryAttributeTypeID() {
        return $this->fieldlist[$this->pdfuryAttributeTypeID];
    }

    /**
     * Set the attributeName in $this->fieldlist
     * @param string $value Database type: varchar; Database column: Attribute_Name
     */
    public function setAttributeName($value) {
        $this->fieldlist[$this->attributeName] = $value;
    }

    /**
     * Return the attributeName from $this->fieldlist
     * @return string Database type: varchar; Database column: Attribute_Name
     */
    public function getAttributeName() {
        return $this->fieldlist[$this->attributeName];
    }

    /**
     * Set the referencedAttributeID in $this->fieldlist
     * @param int $value Database type: int; Database column: referenced_Attribute_ID
     */
    public function setReferencedAttributeID($value) {
        $this->fieldlist[$this->referencedAttributeID] = $value;
    }

    /**
     * Return the referencedAttributeID from $this->fieldlist
     * @return int Database type: int; Database column: referenced_Attribute_ID
     */
    public function getReferencedAttributeID() {
        return $this->fieldlist[$this->referencedAttributeID];
    }
}