<?php
namespace PDFury;

/**
 * Class PdfurySectionQuery
 * @package PDFury
 * @author Eduardo Martos
 */
class PdfurySectionQuery extends FCM_Query {
    /**
     * @var String $sectionName Database type: varchar; Database column: Section_Name
     */
    protected $sectionName;

    /**
     * Constructor for class PdfurySection
     * @param bool|int $primaryValue Primary key value
     */
    public function __construct($primaryValue = false)
    {
        parent::__construct($primaryValue);

        // Table name
        $this->table = 'pdfury_section';

        // Primary key
        $this->primary_key = 'PDFury_Section_ID';

        // Columns
        $this->sectionName = 'Section_Name';

        // Columns
        $this->fieldlist[$this->sectionName] = '';

        // Null fields

    }

    /**
     * Set the sectionName in $this->fieldlist
     * @param String $value Database type: varchar; Database column: Section_Name
     */
    public function setSectionName($value) {
        $this->fieldlist[$this->sectionName] = $value;
    }

    /**
     * Return the sectionName from $this->fieldlist
     * @return String Database type: varchar; Database column: Section_Name
     */
    public function getSectionName() {
        return $this->fieldlist[$this->sectionName];
    }
}