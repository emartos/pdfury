<?php
namespace PDFury;

/**
 * Class PdfuryContainerQuery
 * @package PDFury
 * @author Eduardo Martos
 */
class PdfuryContainerQuery extends FCM_Query {
    /**
     * @var String $containerName Database type: varchar; Database column: Container_Name
     */
    protected $containerName;

    /**
     * Constructor for class PdfuryContainer
     * @param bool|int $primaryValue Primary key value
     */
    public function __construct($primaryValue = false)
    {
        parent::__construct($primaryValue);

        // Table name
        $this->table = 'pdfury_container';

        // Primary key
        $this->primary_key = 'PDFury_Container_ID';

        // Columns
        $this->containerName = 'Container_Name';

        // Columns
        $this->fieldlist[$this->containerName] = '';

        // Null fields

    }

    /**
     * Set the containerName in $this->fieldlist
     * @param String $value Database type: varchar; Database column: Container_Name
     */
    public function setContainerName($value) {
        $this->fieldlist[$this->containerName] = $value;
    }

    /**
     * Return the containerName from $this->fieldlist
     * @return String Database type: varchar; Database column: Container_Name
     */
    public function getContainerName() {
        return $this->fieldlist[$this->containerName];
    }
}