<?php
namespace PDFury;

/**
 * Class PdfuryAttributeTypeQuery
 * @package PDFury
 * @author Eduardo Martos
 */
class PdfuryAttributeTypeQuery extends FCM_Query {
    /**
     * @var String $attributeType Database type: varchar; Database column: Attribute_Type
     */
    protected $attributeType;

    /**
     * Constructor for class PdfuryAttributeType
     * @param bool|int $primaryValue Primary key value
     */
    public function __construct($primaryValue = false) {
        parent::__construct($primaryValue);

        // Table name
        $this->table = 'pdfury_attribute_type';

        // Primary key
        $this->primary_key = 'PDFury_Attribute_Type_ID';

        // Columns
        $this->attributeType = 'Attribute_Type';

        // Columns
        $this->fieldlist[$this->attributeType] = '';

        // Null fields

    }

    /**
     * Set the attributeType in $this->fieldlist
     *
     * @param String $value Database type: varchar; Database column: Attribute_Type
     */
    public function setAttributeType($value) {
        $this->fieldlist[$this->attributeType] = $value;
    }

    /**
     * Return the attributeType from $this->fieldlist
     *
     * @return String Database type: varchar; Database column: Attribute_Type
     */
    public function getAttributeType() {
        return $this->fieldlist[$this->attributeType];
    }
}