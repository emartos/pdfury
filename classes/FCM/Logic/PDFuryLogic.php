<?php
namespace PDFury;

/**
 * Class PDFuryLogic
 * @package PDFury
 * @author Eduardo Martos
 */
class PDFuryLogic {
    /**
     * Create a PDFury Template
     * @param string $templateName
     * @return Mixed
     */
    public function createPDFuryTemplate($templateName) {
        $pdfuryTemplateQuery = new PdfuryTemplateQuery();
        $pdfuryTemplateQuery->setTemplateName($templateName);
        return $pdfuryTemplateQuery->insert();
    }

    /**
     * Create a PDFury container
     * @param string $containerName
     * @return Mixed
     */
    public function createPDFuryContainer($containerName) {
        $pdfuryContainerQuery = new PdfuryContainerQuery();
        $pdfuryContainerQuery->setTemplateName($containerName);
        return $pdfuryContainerQuery->insert();
    }

    /**
     * Create a PDFury attribute type
     * @param string $attributeType
     * @return Mixed
     */
    public function createPDFuryAttributeType($attributeType) {
        $pdfuryAttributeTypeQuery = new PdfuryAttributeTypeQuery();
        $pdfuryAttributeTypeQuery->setTemplateName($attributeType);
        return $pdfuryAttributeTypeQuery->insert();
    }

    /**
     * Create a PDFury section
     * @param string $sectionName
     * @return Mixed
     */
    public function createPDFurySection($sectionName) {
        $pdfurySectionQuery = new PdfurySectionQuery();
        $pdfurySectionQuery->setSectionName($sectionName);
        return $pdfurySectionQuery->insert();
    }

    /**
     * Create a PDFury datasource
     * @param string $datasource
     * @param string $datasourceType
     * @return Mixed
     */
    public function createPDFuryDatasource($datasource, $datasourceType) {
        $pdfuryDatasourceQuery = new PdfuryDatasourceQuery();
        $pdfuryDatasourceQuery->setDatasource($datasource);
        $pdfuryDatasourceQuery->setDatasourceType($datasourceType);
        return $pdfuryDatasourceQuery->insert();
    }

    /**
     * Create a PDFury attribute
     * @param int $pdfuryAttributeTypeID
     * @param null|int $referencedAttributeID
     * @return Mixed
     */
    public function createPDFuryAttribute($pdfuryAttributeTypeID, $referencedAttributeID = null) {
        $pdfuryAttributeQuery = new PdfuryAttributeQuery();
        $pdfuryAttributeQuery->setPDFuryAttributeTypeID($pdfuryAttributeTypeID);
        $pdfuryAttributeQuery->setReferencedAttributeID($referencedAttributeID);
        return $pdfuryAttributeQuery->insert();
    }

    /**
     * Associate an attribute to a template
     * @param int $pdfuryAttributeID
     * @param int $pdfuryTemplateID
     * @param int $pdfurySectionID
     * @param null|int $pdfuryContainerID
     * @return Mixed
     */
    public function associateAttributeToTemplate($pdfuryAttributeID, $pdfuryTemplateID,
                                                 $pdfurySectionID = null, $pdfuryContainerID = null) {
        $pdfuryTemplateToAttributeTypeQuery = new PdfuryTemplateToAttributeQuery();
        $pdfuryTemplateToAttributeTypeQuery->setPDFuryAttributeID($pdfuryAttributeID);
        $pdfuryTemplateToAttributeTypeQuery->setPDFuryTemplateID($pdfuryTemplateID);
        $pdfuryTemplateToAttributeTypeQuery->setPDFurySectionID($pdfurySectionID);
        $pdfuryTemplateToAttributeTypeQuery->setPDFuryContainerID($pdfuryContainerID);
        return $pdfuryTemplateToAttributeTypeQuery->insert();
    }

    /**
     * Associate a datasource to a template
     * @param int $pdfuryDatasourceID
     * @param int $pdfuryTemplateID
     * @return Mixed
     */
    public function associateDatasourceToTemplate($pdfuryDatasourceID, $pdfuryTemplateID) {
        $pdfuryTemplateToDatasourceQuery = new PdfuryTemplateToDatasourceQuery();
        $pdfuryTemplateToDatasourceQuery->setPDFuryDatasourceID($pdfuryDatasourceID);
        $pdfuryTemplateToDatasourceQuery->setPDFuryTemplateID($pdfuryTemplateID);
        return $pdfuryTemplateToDatasourceQuery->insert();
    }

    /**
     * Retrieve the template ID
     * @param $pdfuryTemplateName
     * @return bool
     */
    public function getTemplateID($pdfuryTemplateName) {
        $ret = false;
        $queryString = " PDFury_Template_ID " .
                       "FROM pdfury_template " .
                       "WHERE Template_Name='" . $pdfuryTemplateName . "' ";
        $integration = PDFuryIntegration::getInstance();
        if ($data = $integration->query($queryString)) {
            $data = array_values($data)[0];
            if (isset($data['PDFury_Template_ID'])) {
                $ret = $data['PDFury_Template_ID'];
            }
        }
        return $ret;
    }

    /**
     * Retrieve the attributes associated to a template
     * @param int $pdfuryTemplateID
     * @param null|int $pdfuryAttributeID
     * @param null|int $pdfurySectionID
     * @return array|bool
     */
    public function getAttributes($pdfuryTemplateID, $pdfuryAttributeID = null, $pdfurySectionID = null) {
        $queryString = '* ' .
                       'FROM pdfury_attribute ' .
                       'JOIN pdfury_template_to_attribute ON(pdfury_template_to_attribute.PDFury_Attribute_ID = pdfury_attribute.PDFury_Attribute_ID) ' .
                       'JOIN pdfury_template ON(pdfury_template.PDFury_Template_ID = pdfury_template_to_attribute.PDFury_Template_ID) ' .
                       'JOIN pdfury_attribute_type ON(pdfury_attribute_type.PDFury_Attribute_Type_ID = pdfury_attribute.PDFury_Attribute_Type_ID) ' .
                       'LEFT JOIN pdfury_container ON(pdfury_container.PDFury_Container_ID = pdfury_template_to_attribute.PDFury_Container_ID) ' .
                       'LEFT JOIN pdfury_section ON(pdfury_section.PDFury_Section_ID = pdfury_template_to_attribute.PDFury_Section_ID) ' .
                       'WHERE pdfury_template.PDFury_Template_ID=' . $pdfuryTemplateID . ' ';
        if (!is_null($pdfurySectionID)) {
            $queryString .= 'AND pdfury_template_to_attribute.PDFury_Section_ID=' . $pdfurySectionID;
        }
        if (!is_null($pdfuryAttributeID)) {
            $queryString .= 'AND pdfury_attribute.PDFury_Attribute_ID=' . $pdfuryAttributeID;
        }
        $integration = PDFuryIntegration::getInstance();
        $data = $integration->query($queryString);
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $attributeName = $value['Attribute_Name'];
                $ret[$attributeName] = $value;
            }
        } else {
            $ret = false;
        }
        return $ret;
    }

    /**
     * Retrieve the datasources associated to a template
     * @param int $pdfuryTemplateID
     * @return array|bool
     */
    public function getDatasources($pdfuryTemplateID) {
        $queryString = '* ' .
                       'FROM pdfury_datasource ' .
                       'JOIN pdfury_template_to_datasource ON(pdfury_datasource.PDFury_Datasource_ID = pdfury_template_to_datasource.PDFury_Datasource_ID) ' .
                       'WHERE pdfury_template_to_datasource.PDFury_Template_ID=' . $pdfuryTemplateID . ' ';
        $integration = PDFuryIntegration::getInstance();
        $data = $integration->query($queryString);
        return $data;
    }
}