<div id="content">
    <h2>Vehicle information</h2>
    <div class="float-left">
        <div><strong>Vehicle ID:</strong> {{ Vehicle_ID|default("") }}</label></div>
        <div><strong>Registration date:</strong> {{ Registration_Date|default("") }}</div>
        <div><strong>Mileage:</strong> {{ Mileage|default("") }}</div>
        <div><strong>Make:</strong> {{ Vehicle_Make|default("") }}</div>
        <div><strong>Model:</strong> {{ Vehicle_Model|default("") }}</div>
        <div><strong>Number of gears:</strong> {{ Number_Of_Gears|default("") }}</div>
        <div><strong>Engine size:</strong> {{ Engine_Size|default("") }}</div>
        <div><strong>Power (KW):</strong> {{ Power_KW|default("") }}</div>
    </div>
    <div class="float-right">
        <div><strong>Fuel type:</strong> {{ Fuel_Type|default("") }}</div>
        <div><strong>Drive type:</strong> {{ Drive_Type|default("") }}</div>
        <div><strong>Gearbox:</strong> {{ Gearbox|default("") }}</div>
        <div><strong>Derivative:</strong> {{ Derivative }}</div>
        <div><strong>Number of seats:</strong> {{ Number_Of_Seats|default("") }}</div>
        <div><strong>Number of doors:</strong> {{ Number_Of_Doors|default("") }}</div>
        <div><strong>Color:</strong> {{ Color }}</div>
        <div><strong>Body type:</strong> {{ Body_Type|default("") }}</div>
    </div>
    {% if WholesalePrice.Wholesale_Price != false %}
    <div class="clear isolated-field"><strong>Wholesale price:</strong> {{ WholesalePrice.Wholesale_Price|first }}</div>
    {% endif %}
    <div class="clear"></div>
    {% if Vehicle_Equipment != false %}
    <h2>Equipment</h2>
    <div>
        <ul>
            {% if Vehicle_Equipment is iterable %}
                {% for Vehicle_Equipment_Value in Vehicle_Equipment %}
                <li>{{ Vehicle_Equipment_Value|e }}</li>
                {% endfor %}
            {% else %}
            <li>{{ Vehicle_Equipment }}</li>
            {% endif %}
        </ul>
    </div>
    {% endif %}
    {% if VehicleBrochureImages.Photo != false %}
    <h2>Images</h2>
    <div>
        {% if VehicleBrochureImages.Photo is iterable %}
            {% for Photo_Key,Photo_Value in VehicleBrochureImages.Photo %}
                {% if Photo_Value != false %}
                <div class="thumbnail-wrapper"><img src="{{ Photo_Value|e }}" class="thumbnail" /></div>
                {% endif %}
            {% endfor %}
        {% else %}
        <div class="thumbnail-wrapper"><img src="{{ VehicleBrochureImages.Photo }}" class="thumbnail" /></div>
        {% endif %}
    </div>
    {% endif %}
</div>