<div id="content">
    <h2>Fahrzeug Information</h2>
    <div class="float-left">
        <div><strong>Fzg-ID:</strong> {{ Vehicle_ID|default("") }}</strong> </div>
        <div><strong>Zulassungsdatum:</strong> {{ Registration_Date|default("") }}</div>
        <div><strong>Nach Kilometerstand:</strong> {{ Mileage|default("") }}</div>
        <div><strong>Hersteller:</strong> {{ Vehicle_Make|default("") }}</div>
        <div><strong>Modell:</strong> {{ Vehicle_Model|default("") }}</div>
        <div><strong>Anzahl Gänge:</strong> {{ Number_Of_Gears|default("") }}</div>
        <div><strong>Hubraum:</strong> {{ Engine_Size|default("") }}</div>
        <div><strong>Leistung (KW):</strong> {{ Power_KW|default("") }}</div>
    </div>
    <div class="float-right">
        <div><strong>Karosserietyp:</strong> {{ Body_Type|default("") }}</div>
        <div><strong>Farbe:</strong> {{ Color|default("") }}</div>
        <div><strong>Kraftstoffart:</strong> {{ Fuel_Type|default("") }}</div>
        <div><strong>Antriebsart:</strong> {{ Drive_Type|default("") }}</div>
        <div><strong>Getriebeart:</strong> {{ Gearbox|default("") }}</div>
        <div><strong>Modellvariante:</strong> {{ Derivative|default("") }}</div>
        <div><strong>Anzahl Sitze:</strong> {{ Number_Of_Seats|default("") }}</div>
        <div><strong>Anzahl der Türen:</strong> {{ Number_Of_Doors|default("") }}</div>
    </div>
    {% if WholesalePrice.Wholesale_Price != false %}
    <div class="clear isolated-field"><strong>Wholesale-Preis:</strong> {{ WholesalePrice.Wholesale_Price|first }}</div>
    {% endif %}
    <div class="clear"></div>
    {% if Vehicle_Equipment != false %}
    <h2>Ausstattung</h2>
    <div>
        <ul>
            {% if Vehicle_Equipment is iterable %}
                {% for Vehicle_Equipment_Value in Vehicle_Equipment %}
                <li>{{ Vehicle_Equipment_Value|e }}</li>
                {% endfor %}
            {% else %}
            <li>{{ Vehicle_Equipment }}</li>
            {% endif %}
        </ul>
    </div>
    {% endif %}
    {% if VehicleBrochureImages.Photo != false %}
    <h2>Bilder</h2>
    <div>
        {% if VehicleBrochureImages.Photo is iterable %}
            {% for Photo_Key,Photo_Value in VehicleBrochureImages.Photo %}
                {% if Photo_Value != false %}
                <div class="thumbnail-wrapper"><img src="{{ Photo_Value|e }}" class="thumbnail" /></div>
                {% endif %}
            {% endfor %}
        {% else %}
        <div class="thumbnail-wrapper"><img src="{{ VehicleBrochureImages.Photo }}" class="thumbnail" /></div>
        {% endif %}
    </div>
    {% endif %}
</div>
