<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>{{ title }}</title>
    {% for file in css %}
    <link href="{{ file|e }}" rel="stylesheet" type="text/css" />
    {% endfor %}
</head>
<body>