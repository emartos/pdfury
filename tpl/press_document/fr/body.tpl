<div id="content">
    <h1 id="title">Mise à disposition d’un véhicule<br />Terbeschikkingstelling van een voertuig</h1>
    <div class="clear"></div>
    <div style="width: 100%;">
        <div class="line-separator">
            <div class="paired-field"><strong>Naam/nom:</strong> {{ Driver_First_Name }} {{ Driver_Last_Name }}</div>
            <div class="paired-field"><strong>Bedrijf/société:</strong> {{ Driver_Company_Name }}</div>
            <div class="clear"></div>
        </div>
        <div class="line-separator">
            <div class="paired-field"><strong>ID kaart/carte ID:</strong> {{ Driver_Identity_Card }}</div>
            <div class="paired-field"><strong>Tel./E-mail:</strong> {{ Driver_Mobile }}{% if Driver_Mobile is not empty and Driver_Phone is not empty %}/{% endif %}{{ Driver_Phone }}{% if (Driver_Mobile is not empty or Driver_Phone is not empty) and Driver_Email is not empty %}/{% endif %}{{ Driver_Email }}</strong></div>
            <div class="clear"></div>
        </div>
        <div class="line-separator">
            <div class="paired-field"><strong>Model/modèle:</strong> {{ Vehicle_Model }}</div>
            <div class="paired-field"><strong>Nummerplaat/plaque:</strong> {{ Licence_Number }}</div>
            <div class="clear"></div>
        </div>
        <div class="line-separator">
            <div class="paired-field"><strong>Période/durée:</strong> {{ Reservation_Start_Date }}</div>
            <div class="paired-field"><strong>tot/au:</strong> {{ Reservation_End_Date }}</div>
            <div class="clear"></div>
        </div>
        <div>
            <div class="margin-bottom"><strong>Opmerkingen/remarques:</strong> {{ Reservation_Description }}</div>
            <div class="margin-bottom"><strong>La forme de remise/Type overdracht:</strong> {{ Handover }} / {{ Handover_NL }}</div>
            <div><strong>Annexe/Bijlage:</strong> Car check</div>
        </div>
    </div>
    <div>
        <h2>ALGEMENE VOORWAARDEN</h2>
        <ul>
            <li>Deze wagen wordt ter beschikking gesteld in perfecte staat en zal in dezelfde staat terug worden gebracht.</li>
            <li>Alle boetes, kosten en belasting tengevolge verkeer  en parkeerovertredingen of andere, zijn ten laste van de gebruiker van het voertuig, alsook, bij ongeval indien van toepassing, de vrijstelling van 4% van de waarde van het voertuig. Een ongeval dient steeds binnen de 24u verklaard te worden. Indien er geen gevolg wordt gegeven aan de vraag tot betaling, zal KMB dit bedrag doorfacturen aan de gebruiker.</li>
            <li>Deze wagen wordt ter beschikking gesteld voor een bepaalde duur zoals hierboven vermeld.</li>
            <li>De wagen mag in geen enkel geval bestuurd worden door een andere persoon dan deze hierboven vermeld.</li>
            <li>Het voertuig dient te worden ingeleverd met de boorddocumenten of een attest van verlies.</li>
            <li>Het is strikt verboden om te roken in de wagen!</li>
        </ul>
        <h2>CONDITIONS GÉNÉRALES</h2>
        <ul>
            <li>Ce véhicule est  mis à disposition dans un parfait état, et sera ramené dans le même état.</li>
            <li>Toutes les amendes, frais et les taxes suite aux infractions au code de la route sont à charge de l’utilisateur ainsi que l’éventuelle franchise de 4% de la valeur du véhicule. En cas d’accident une déclaration doit nous etre envoyé dans les 24h. Si aucune suite n’est donnée à la demande de paiement, KMB se verra dans  l’obligation de refacturer le montant à la personne mentionnée ci-dessus.</li>
            <li>Ce véhicule est prêté pour une durée reprise ci-dessus.</li>
            <li>Ce véhicule ne peut en aucun cas être conduit par une autre personne que celle mentionnée ci-dessus.</li>
            <li>Le véhicule doit être restitué avec les documents de bord, ou une attestation de perte.</li>
            <li>Il est strictement interdit de fumer dans le véhicule!</li>
        </ul>
        <p>De ontlener verklaart kennis te hebben genomen van en aanvaardt de algemene voorwaarden.</p>
        <p>L’emprunteur déclare avoir lu et accepte les conditions générales.</p>
    </div>
    <div class="signature">
        <div class="float-left">
            <p>Handtekening van de ontlener<br />
                Signature de l’emprunteur</p>
        </div>
        <div class="float-right">
            <p>VAB Fleet Services N.V. pour/voor<br />
                Kia Motors Belgium S.A./N.V.</p>
        </div>
    </div>
</div>