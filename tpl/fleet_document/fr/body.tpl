<div id="content">
    <h1 id="title">Mise à disposition d’un véhicule<br />Terbeschikkingstelling van een voertuig</h1>
    <div class="clear"></div>
    <table>
        <thead>
            <tr>
                <th colspan="4">Contact details</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:20%">Nom/naam:</td>
                <td style="width:30%">{{ Driver_First_Name }} {{ Driver_Last_Name }}</td>
                <td style="width:20%">Société/bedrijf:</td>
                <td style="width:30%">{{ Driver_Company_Name }}</td>
            </tr>
            <tr>
                <td style="width:20%">Tel:</td>
                <td style="width:30%">{{ Driver_Phone }}</td>
                <td style="width:20%">E-mail:</td>
                <td style="width:30%">{{ Driver_Email }}</td>
            </tr>
            <tr>
                <td style="width:20%">GSM:</td>
                <td style="width:30%">{{ Driver_Mobile }}</td>
                <td style="width:20%">Carte ID/ ID kaart:</td>
                <td style="width:30%">{{ Driver_Identity_Card }}</td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <th colspan="4">Reservation details</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="width:20%">Marque/merk:</td>
                <td style="width:30%">{{ Vehicle_Make }}</td>
                <td style="width:20%">Plaat/plaque:</td>
                <td style="width:30%">{{ Licence_Number }}</td>
            </tr>
            <tr>
                <td style="width:20%">Modèle/model:</td>
                <td style="width:30%">{{ Vehicle_Model }}</td>
                <td style="width:20%">VIN:</td>
                <td style="width:30%">{{ VIN }}</td>
            </tr>
            <tr>
                <td style="width:20%">Du/van:</td>
                <td style="width:30%">{{ Reservation_Start_Date }}</td>
                <td style="width:20%">Au/tot:</td>
                <td style="width:30%">{{ Reservation_End_Date }}</td>
            </tr>
        </tbody>
    </table>
    <div class="separator">La forme de remise/Type overdracht: {{ Handover }} / {{ Handover_NL }}</div>
    <div class="separator">Annexe/Bijlage: Car check</div>
    <div>
        <h2>ALGEMENE VOORWAARDEN</h2>
        <ul>
            <li>Deze wagen wordt ter beschikking gesteld in perfecte staat en zal in dezelfde staat terug worden gebracht.</li>
            <li>Alle boetes, kosten en belasting tengevolge verkeer  en parkeerovertredingen of andere, zijn ten laste van de gebruiker van het voertuig, alsook, bij ongeval indien van toepassing, de vrijstelling van 4% van de waarde van het voertuig. Een ongeval dient steeds binnen de 24u verklaard te worden. Indien er geen gevolg wordt gegeven aan de vraag tot betaling, zal KMB dit bedrag doorfacturen aan de gebruiker.</li>
            <li>Deze wagen wordt ter beschikking gesteld voor een bepaalde duur zoals hierboven vermeld.</li>
            <li>De wagen mag in geen enkel geval bestuurd worden door een andere persoon dan deze hierboven vermeld.</li>
            <li>Voertuig dient steeds terug volgetankt binnengeleverd te worden of tenminste op niveau zoals ontvangen, in tegenstelling zal u dit gefactureerd worden.</li>
            <li>Het voertuig dient te worden ingeleverd met de boorddocumenten of een attest van verlies.</li>
            <li>Het is strikt verboden om te roken in de wagen!</li>
        </ul>
        <h2>CONDITIONS GÉNÉRALES</h2>
        <ul>
            <li>Ce véhicule est  mis à disposition dans un parfait état, et sera ramené dans le même état.</li>
            <li>Toutes les amendes, frais et les taxes suite aux infractions au code de la route sont à charge de l’utilisateur ainsi que l’éventuelle franchise de 4% de la valeur du véhicule. En cas d’accident une déclaration doit nous etre envoyé dans les 24h. Si aucune suite n’est donnée à la demande de paiement, KMB se verra dans  l’obligation de refacturer le montant à la personne mentionnée ci-dessus.</li>
            <li>Ce véhicule est prêté pour une durée reprise ci-dessus.</li>
            <li>Ce véhicule ne peut en aucun cas être conduit par une autre personne que celle mentionnée ci-dessus.</li>
            <li>La voiture doit nous être restituée avec le carburant plein ou au moins au niveau tel que reçu, si tel n’était pas le cas, une facture vous sera adressée.</li>
            <li>Le véhicule doit être restitué avec les documents de bord, ou une attestation de perte.</li>
            <li>Il est strictement interdit de fumer dans le véhicule!</li>
        </ul>
        <p>De ontlener verklaart kennis te hebben genomen van en aanvaardt de algemene voorwaarden.</p>
        <p>L’emprunteur déclare avoir lu et accepte les conditions générales.</p>
    </div>
    <div class="signature">
        <div class="float-left">
            <p>Handtekening van de ontlener<br />
                Signature de l’emprunteur</p>
        </div>
        <div class="float-right">
            <p>VAB Fleet Services N.V. pour/voor<br />
                Kia Motors Belgium S.A./N.V.</p>
        </div>
    </div>
</div>