-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 192.168.5.108    Database: fcm4
-- ------------------------------------------------------
-- Server version	5.5.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pdfury_attribute`
--

DROP TABLE IF EXISTS `pdfury_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfury_attribute` (
  `PDFury_Attribute_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PDFury_Attribute_Type_ID` int(11) NOT NULL,
  `Attribute_Name` varchar(255) NOT NULL,
  `Referenced_Attribute_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PDFury_Attribute_ID`),
  UNIQUE KEY `Attribute_Name_UNIQUE` (`Attribute_Name`),
  KEY `pdfury_attribute_I` (`PDFury_Attribute_Type_ID`),
  CONSTRAINT `FK_PDFury_Attribute_Type_ID_idx` FOREIGN KEY (`PDFury_Attribute_Type_ID`) REFERENCES `pdfury_attribute_type` (`PDFury_Attribute_Type_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdfury_attribute`
--

LOCK TABLES `pdfury_attribute` WRITE;
/*!40000 ALTER TABLE `pdfury_attribute` DISABLE KEYS */;
INSERT INTO `pdfury_attribute` VALUES (1,2,'Reference1',NULL),(2,5,'Vehicle_ID',NULL),(3,6,'Registration_Date',NULL),(4,5,'Mileage',NULL),(5,5,'Vehicle_Make',NULL),(6,5,'Vehicle_Model',NULL),(7,7,'Wholesale_Price',NULL),(8,5,'Number_Of_Gears',NULL),(9,5,'Engine_Size',NULL),(10,5,'Power_KW',NULL),(11,3,'Fuel_Type',NULL),(12,3,'Drive_Type',NULL),(13,3,'Gearbox',NULL),(14,5,'Derivative',NULL),(15,5,'Number_Of_Seats',NULL),(16,5,'Number_Of_Doors',NULL),(17,3,'Color',NULL),(18,3,'Vehicle_Equipment',NULL),(19,5,'Logo',NULL),(20,9,'Photo',NULL),(21,3,'Body_Type',NULL),(23,5,'VIN',NULL),(24,3,'Handover',NULL),(25,5,'Driver_First_Name',NULL),(26,5,'Driver_Last_Name',NULL),(27,5,'Driver_Company_Name',NULL),(28,5,'Driver_Identity_Card',NULL),(29,5,'Driver_Email',NULL),(30,5,'Driver_Mobile',NULL),(31,5,'Driver_Phone',NULL),(32,6,'Reservation_Start_Date',NULL),(33,6,'Reservation_End_Date',NULL),(34,8,'Reservation_Description',NULL),(35,5,'Licence_Number',NULL),(36,5,'Handover_NL',NULL);
/*!40000 ALTER TABLE `pdfury_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdfury_attribute_type`
--

DROP TABLE IF EXISTS `pdfury_attribute_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfury_attribute_type` (
  `PDFury_Attribute_Type_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Attribute_Type` varchar(255) NOT NULL,
  PRIMARY KEY (`PDFury_Attribute_Type_ID`),
  UNIQUE KEY `Attribute_Type_UNIQUE` (`Attribute_Type`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdfury_attribute_type`
--

LOCK TABLES `pdfury_attribute_type` WRITE;
/*!40000 ALTER TABLE `pdfury_attribute_type` DISABLE KEYS */;
INSERT INTO `pdfury_attribute_type` VALUES (3,'PDFURY_ATTR_TYPE_CODEBASE'),(7,'PDFURY_ATTR_TYPE_CURRENCY'),(6,'PDFURY_ATTR_TYPE_DATE'),(8,'PDFURY_ATTR_TYPE_HTML'),(9,'PDFURY_ATTR_TYPE_IMAGE'),(2,'PDFURY_ATTR_TYPE_QR'),(1,'PDFURY_ATTR_TYPE_REFERENCED_FIELD'),(5,'PDFURY_ATTR_TYPE_TEXT'),(4,'PDFURY_ATTR_TYPE_UNKOWN_IF_UNDEFINED');
/*!40000 ALTER TABLE `pdfury_attribute_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdfury_container`
--

DROP TABLE IF EXISTS `pdfury_container`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfury_container` (
  `PDFury_Container_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Container_Name` varchar(255) NOT NULL,
  PRIMARY KEY (`PDFury_Container_ID`),
  UNIQUE KEY `Container_Name_UNIQUE` (`Container_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdfury_container`
--

LOCK TABLES `pdfury_container` WRITE;
/*!40000 ALTER TABLE `pdfury_container` DISABLE KEYS */;
INSERT INTO `pdfury_container` VALUES (1,'WholesalePrice');
/*!40000 ALTER TABLE `pdfury_container` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdfury_datasource`
--

DROP TABLE IF EXISTS `pdfury_datasource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfury_datasource` (
  `PDFury_Datasource_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Datasource` text NOT NULL,
  `Datasource_Type` varchar(255) NOT NULL,
  `Datasource_Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PDFury_Datasource_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdfury_datasource`
--

LOCK TABLES `pdfury_datasource` WRITE;
/*!40000 ALTER TABLE `pdfury_datasource` DISABLE KEYS */;
INSERT INTO `pdfury_datasource` VALUES (1,'SELECT vehicle.Vehicle_ID AS Vehicle_ID,\n        vehicle.VIN AS VIN,\n        vehicle.First_Reg_Date AS Registration_Date,\n        vehicle.Mileage_Actual AS Mileage,\n        vehicle_make.Name AS Vehicle_Make,\n        vehicle_model.Name AS Vehicle_Model,\n        price_actual.Wholesale_Price AS Wholesale_Price,\n        vehicle_engine.Number_Of_Gears AS Number_Of_Gears,\n        vehicle_engine.Engine_Size AS Engine_Size,\n        vehicle_engine.Power_KW AS Power_KW,\n        fuel_type.Translation_Code AS Fuel_Type,\n        drive_type.Translation_Code AS Drive_Type,\n        gearbox.Translation_Code AS Gearbox,\n        derivative.Name AS Derivative,\n        interior.Number_Of_Seats AS Number_Of_Seats,\n        exterior.Number_Of_Doors AS Number_Of_Doors,\n        color.Translation_Code AS Color,\n        body_type.Translation_Code AS Body_Type\n    FROM\n    	vehicle\n    JOIN\n    	price_actual ON price_actual.Vehicle_ID = vehicle.Vehicle_ID\n    JOIN\n    	make_to_model ON make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID\n    JOIN\n    	vehicle_make ON vehicle_make.Make_ID = make_to_model.Make_ID\n    JOIN\n    	vehicle_model ON vehicle_model.Vehicle_Model_ID = make_to_model.Model_ID\n    JOIN\n    	vehicle_details ON vehicle_details.Vehicle_ID = vehicle.Vehicle_ID\n    JOIN\n    	vehicle_engine ON vehicle_engine.Vehicle_Details_ID = vehicle_details.Vehicle_Details_ID\n    JOIN\n    	fuel_type ON fuel_type.Fuel_Type_ID = vehicle_engine.Fuel_Type_ID\n    JOIN\n    	drive_type ON drive_type.Drive_Type_ID = vehicle_engine.Drive_Type_ID\n    JOIN\n    	gearbox ON gearbox.gearbox_id = vehicle_engine.gearbox_id\n    JOIN\n    	derivative ON derivative.derivative_id = vehicle_details.Derivative_ID\n    JOIN\n    	interior ON interior.Vehicle_Details_ID = vehicle_details.Vehicle_Details_ID\n    JOIN\n    	exterior ON exterior.Vehicle_Details_ID = vehicle_details.Vehicle_Details_ID\n    JOIN\n    	vehicle_type ON vehicle_type.Vehicle_Type_ID = vehicle_details.Vehicle_Type_ID\n    JOIN\n    	color ON color.Color_ID = vehicle.Color_ID\n	JOIN\n		body_type ON body_type.Body_Type_ID = exterior.Body_Type_ID\n    WHERE\n    	vehicle.Vehicle_ID=:vehicleid:','PDFURY_DATASOURCE_QUERY','Query to retrieve the vehicle details for the vehicle brochure.'),(2,'SELECT \r     vehicle_equipment.Translation_Code AS Vehicle_Equipment\r FROM\r 	vehicle\r JOIN\r 	vehicle_to_equipment ON vehicle_to_equipment.Vehicle_ID = vehicle.Vehicle_ID\r JOIN\r 	vehicle_equipment ON vehicle_equipment.Equipment_ID = vehicle_to_equipment.Equipment_ID\r WHERE\r 	vehicle.vehicle_ID = :vehicleid:','PDFURY_DATASOURCE_QUERY','Query to retrieve the vehicle equipment information.'),(3,'SELECT \n	Logo\nFROM\n	partner\nWHERE\n	Partner_ID = :partnerid:','PDFURY_DATASOURCE_QUERY','Query to retrieve the partner logo.'),(4,'SELECT CONCAT(\n	document.docpath,\n    document.Partner_ID, \"/\",\n    vehicle_to_document.Vehicle_ID, \"/\",\n    document.File_Name\n    ) AS Photo\nFROM\n	document\nJOIN\n	vehicle_to_document ON vehicle_to_document.Document_ID = document.Document_ID\nJOIN\n	file_type ON file_type.File_Type_ID = document.File_Type_ID\nWHERE\n	vehicle_to_document.Vehicle_ID = :vehicleid:\n    AND document.Document_Template_ID IN (2, 41)\n    AND document.deleted = 0','PDFURY_DATASOURCE_QUERY','Query to retrieve the vehicle pictures.'),(5,'SELECT \n 	vehicle.Vehicle_ID AS Vehicle_ID,\n     vehicle.Licence_No_Actual AS Licence_Number,\n     vehicle.VIN AS VIN,\n     vehicle.First_Reg_Date AS Registration_Date,\n     vehicle_make.Name AS Vehicle_Make,\n     vehicle_model.Name AS Vehicle_Model,\n     reservation.Handover_Type AS Handover,\n     (SELECT NL FROM codebase WHERE Translation_Code = reservation.Handover_Type) AS Handover_NL,\n     reservation.Start_Date AS Reservation_Start_Date,\n     reservation.End_Date AS Reservation_End_Date,\n     reservation.Description AS Reservation_Description,\n     CONCAT(person.First_Name, \'\', person.Sec_First_Name) AS Driver_First_Name,\n     CONCAT(person.Last_Name, \'\', person.Sec_Last_Name) AS Driver_Last_Name,\n     person.Company_Name AS Driver_Company_Name,\n 	CONCAT(person.Identitiy_Card_Num, \'\', person.Sec_Identitiy_Card_Num) AS Driver_Identity_Card,\n     e_mail.E_Mail AS Driver_Email,\n     mobile_tel.Mobile_Tel AS Driver_Mobile,\n     phone_number.Phone AS Driver_Phone\n FROM\n 	vehicle\n JOIN\n 	make_to_model ON make_to_model.Make_To_Model_ID = vehicle.Make_To_Model_ID\n JOIN\n 	vehicle_make ON vehicle_make.Make_ID = make_to_model.Make_ID\n JOIN\n 	vehicle_model ON vehicle_model.Vehicle_Model_ID = make_to_model.Model_ID\n JOIN\n 	reservation ON reservation.Vehicle_ID = vehicle.Vehicle_ID\n JOIN\n 	person ON person.Person_ID = reservation.Reservator_ID\n LEFT JOIN\n 	person_to_e_mail ON person_to_e_mail.Person_ID = person.Person_ID\n LEFT JOIN\n 	e_mail ON e_mail.E_Mail_ID = person_to_e_mail.E_Mail_ID\n LEFT JOIN\n 	person_to_mobile ON person_to_mobile.Person_ID = person.Person_ID\n LEFT JOIN\n 	mobile_tel ON mobile_tel.Mobile_Tel_ID = person_to_mobile.Mobile_Tel_ID\n LEFT JOIN\n 	person_to_phone_number ON person_to_phone_number.Person_ID = person.Person_ID\n LEFT JOIN\n 	phone_number ON phone_number.Phone_Number_ID = person_to_phone_number.Phone_Number_ID\n WHERE\n 	vehicle.Vehicle_ID=:vehicleid: AND reservation.Reservation_ID=:reservationid:','PDFURY_DATASOURCE_QUERY','Query to retrieve vehicles and reservation details for poolcar documentation.');
/*!40000 ALTER TABLE `pdfury_datasource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdfury_section`
--

DROP TABLE IF EXISTS `pdfury_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfury_section` (
  `PDFury_Section_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Section_Name` varchar(255) NOT NULL,
  PRIMARY KEY (`PDFury_Section_ID`),
  UNIQUE KEY `Section_Name_UNIQUE` (`Section_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdfury_section`
--

LOCK TABLES `pdfury_section` WRITE;
/*!40000 ALTER TABLE `pdfury_section` DISABLE KEYS */;
INSERT INTO `pdfury_section` VALUES (1,'VehicleBrochureImages');
/*!40000 ALTER TABLE `pdfury_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdfury_template`
--

DROP TABLE IF EXISTS `pdfury_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfury_template` (
  `PDFury_Template_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Template_Name` varchar(255) NOT NULL,
  `Template_Title` varchar(255) DEFAULT NULL,
  `PDF_Command_Parameters` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PDFury_Template_ID`),
  UNIQUE KEY `Template_Name_UNIQUE` (`Template_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdfury_template`
--

LOCK TABLES `pdfury_template` WRITE;
/*!40000 ALTER TABLE `pdfury_template` DISABLE KEYS */;
INSERT INTO `pdfury_template` VALUES (1,'vehiclebrochure','Vehicle Brochure',NULL),(2,'fleet_document','Fleet document','--ignore-load-errors --encoding utf-8 --print-media-type --margin-left 12mm --margin-right 10mm --margin-top 40mm --margin-bottom 15mm --header-spacing 8'),(3,'press_document','Press document','--ignore-load-errors --encoding utf-8 --print-media-type --margin-left 12mm --margin-right 10mm --margin-top 40mm --margin-bottom 15mm --header-spacing 8');
/*!40000 ALTER TABLE `pdfury_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdfury_template_to_attribute`
--

DROP TABLE IF EXISTS `pdfury_template_to_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfury_template_to_attribute` (
  `PDFury_Template_To_Attribute_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PDFury_Template_ID` int(11) NOT NULL,
  `PDFury_Attribute_ID` int(11) NOT NULL,
  `PDFury_Section_ID` int(11) DEFAULT NULL,
  `PDFury_Container_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PDFury_Template_To_Attribute_ID`),
  KEY `pdfury_template_to_attribute_I4` (`PDFury_Container_ID`),
  KEY `pdfury_template_to_attribute_I3` (`PDFury_Section_ID`),
  KEY `pdfury_template_to_attribute_I2` (`PDFury_Attribute_ID`),
  KEY `pdfury_template_to_attribute_I` (`PDFury_Template_ID`),
  CONSTRAINT `FK_PDFury_Attribute_ID_idx` FOREIGN KEY (`PDFury_Attribute_ID`) REFERENCES `pdfury_attribute` (`PDFury_Attribute_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PDFury_Container_ID_idx` FOREIGN KEY (`PDFury_Container_ID`) REFERENCES `pdfury_container` (`PDFury_Container_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PDFury_Section_ID_idx` FOREIGN KEY (`PDFury_Section_ID`) REFERENCES `pdfury_section` (`PDFury_Section_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdfury_template_to_attribute`
--

LOCK TABLES `pdfury_template_to_attribute` WRITE;
/*!40000 ALTER TABLE `pdfury_template_to_attribute` DISABLE KEYS */;
INSERT INTO `pdfury_template_to_attribute` VALUES (1,1,3,NULL,NULL),(2,1,4,NULL,NULL),(3,1,5,NULL,NULL),(4,1,6,NULL,NULL),(5,1,7,NULL,1),(6,1,8,NULL,NULL),(7,1,9,NULL,NULL),(8,1,10,NULL,NULL),(9,1,11,NULL,NULL),(10,1,12,NULL,NULL),(11,1,13,NULL,NULL),(12,1,14,NULL,NULL),(13,1,15,NULL,NULL),(14,1,16,NULL,NULL),(15,1,17,NULL,NULL),(16,1,18,NULL,NULL),(17,1,19,NULL,NULL),(18,1,1,NULL,NULL),(19,1,20,1,NULL),(20,1,21,NULL,NULL),(22,1,2,NULL,NULL),(75,2,2,NULL,NULL),(76,2,3,NULL,NULL),(77,2,4,NULL,NULL),(78,2,6,NULL,NULL),(79,2,23,NULL,NULL),(80,2,24,NULL,NULL),(81,2,25,NULL,NULL),(82,2,26,NULL,NULL),(83,2,27,NULL,NULL),(84,2,28,NULL,NULL),(85,2,29,NULL,NULL),(86,2,30,NULL,NULL),(87,2,31,NULL,NULL),(88,2,32,NULL,NULL),(89,2,33,NULL,NULL),(90,2,34,NULL,NULL),(91,3,2,NULL,NULL),(92,3,3,NULL,NULL),(93,3,4,NULL,NULL),(94,3,6,NULL,NULL),(95,3,23,NULL,NULL),(96,3,24,NULL,NULL),(97,3,25,NULL,NULL),(98,3,26,NULL,NULL),(99,3,27,NULL,NULL),(100,3,28,NULL,NULL),(101,3,29,NULL,NULL),(102,3,30,NULL,NULL),(103,3,31,NULL,NULL),(104,3,32,NULL,NULL),(105,3,33,NULL,NULL),(106,3,34,NULL,NULL),(107,2,35,NULL,NULL),(108,3,35,NULL,NULL),(109,2,5,NULL,NULL),(110,3,5,NULL,NULL),(111,2,36,NULL,NULL),(112,3,36,NULL,NULL);
/*!40000 ALTER TABLE `pdfury_template_to_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pdfury_template_to_datasource`
--

DROP TABLE IF EXISTS `pdfury_template_to_datasource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pdfury_template_to_datasource` (
  `PDFury_Template_To_Datasource_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PDFury_Template_ID` int(11) NOT NULL,
  `PDFury_Datasource_ID` int(11) NOT NULL,
  PRIMARY KEY (`PDFury_Template_To_Datasource_ID`),
  KEY `pdfury_template_to_datasource_I2` (`PDFury_Datasource_ID`),
  KEY `pdfury_template_to_datasource_I` (`PDFury_Template_ID`),
  CONSTRAINT `FK_PDFury_Datasource_ID_idx` FOREIGN KEY (`PDFury_Datasource_ID`) REFERENCES `pdfury_datasource` (`PDFury_Datasource_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PDFury_Template_ID_idx` FOREIGN KEY (`PDFury_Template_ID`) REFERENCES `pdfury_template` (`PDFury_Template_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pdfury_template_to_datasource`
--

LOCK TABLES `pdfury_template_to_datasource` WRITE;
/*!40000 ALTER TABLE `pdfury_template_to_datasource` DISABLE KEYS */;
INSERT INTO `pdfury_template_to_datasource` VALUES (1,1,2),(2,1,3),(3,1,4),(5,1,1),(6,2,5),(7,3,5);
/*!40000 ALTER TABLE `pdfury_template_to_datasource` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-14  8:45:05
